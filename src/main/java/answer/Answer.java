package answer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Answer {

    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;

    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }

    public List<Integer> getAnswer() {
        return this.answer;
    }

    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }

    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }

    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        // Need to be implemented
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream(filePath.toString());
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String number = reader.readLine();
        String [] numberArr = number.split("");
        List<Integer> numberList = new ArrayList<>();
        for (String s : numberArr) {
            numberList.add(Integer.parseInt(s));
        }
        return numberList;
    }

    private List<Integer> generateRandomAnswer() {
        // Need to be implemented
        Random random = new Random();
        StringBuilder newAnswer = new StringBuilder();
        while (newAnswer.length() != 4) {
            String temp = random.nextInt(10) + "";
            if (!newAnswer.toString().contains(temp))
                newAnswer.append(temp);
        }
        String [] numberArr = newAnswer.toString().split("");
        List<Integer> numberList = new ArrayList<>();
        for (String s : numberArr) {
            numberList.add(Integer.parseInt(s));
        }
        return numberList;
    }

    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        // Need to be implemented
        if (answer.size() != 4) {
            throw new InvalidAnswerException("should be four digits.");
        }
        if (answer.stream().distinct().count() != 4) {
            throw new InvalidAnswerException("should not have duplicate number.");
        }
    }
}
