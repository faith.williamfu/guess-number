package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {

    private Answer answer;
    private Map<String, String> guessResult;

    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }

    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }

    public boolean isOver() {
        // Need to be implemented
        return this.guessResult.size() >= 6 || this.guessResult.containsValue("4A0B");
    }

    public String getResult() {
        // Need to be implemented
        StringBuilder result = new StringBuilder();
        GuessResult guessResult = new GuessResult(this.guessResult);
        result.append(guessResult.getResult());
        result.append("\n");
        if (this.guessResult.containsValue("4A0B")) {
            result.append("Congratulations, you win!");
        } else if (this.guessResult.size() >= 6 && !this.guessResult.containsValue("4A0B")){
            result.append("Unfortunately, you have no chance, the answer is 1234!");
        }
        return result.toString();
    }

    private int getBSize(List<Integer> numbers) {
        // Need to be implemented
        int B = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (!numbers.get(i).equals(answer.getAnswer().get(i)) && answer.getAnswer().contains(numbers.get(i))) {
                B ++;
            }}
        return B;
    }

    private int getASize(List<Integer> numbers) {
        // Need to be implemented
        int A = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i).equals(answer.getAnswer().get(i))) {
                A ++;
            }}
        return A;
    }
}
