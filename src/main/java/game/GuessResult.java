package game;

import java.util.Map;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;

    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }


    public String getResult() {
        // Need to be implemented
        StringBuilder result = new StringBuilder();
        result.append(guessRecord.toString().replace("{","").replace("}","").replace("=", " ").replace(", ","\n"));
        return result.toString();
    }

    public GameResult getGameResult() {
        // Need to be implemented
        GameResult result;
        if (guessRecord.size() >= CHANCE_LIMIT && !guessRecord.containsValue("4A0B")) {
            result = GameResult.LOST;
        } else if (guessRecord.containsValue("4A0B")) {
            result = GameResult.WIN;
        } else {
            result = GameResult.NORMAL;
        }
        return result;
    }
}
